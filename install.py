import platform
import sqlite3
from datetime import date
from pathlib import Path

sys_ver = platform.system()
os_ver = platform.release()



if __name__ == '__main__':
    print(f"{sys_ver}{os_ver} staring check system enviroment...")
    if_file_exist()

conn = sqlite3.connect("Hangman.db")
c = conn.cursor()

# Create table
c.execute('''CREATE TABLE Version
            ([Ver] INTEGER, [Data] DATETIME)''')
# Create table
c.execute('''CREATE TABLE Categories
             ([IdCategory] INTEGER PRIMARY KEY AUTOINCREMENT,[Name] text UNIQUE)''')
# Create table
c.execute('''CREATE TABLE Ranking
             ([IdRank] INTEGER PRIMARY KEY AUTOINCREMENT,[Player] TEXT, [Score] INT, [GameData] DATETIME)''')
# Create table
c.execute('''CREATE TABLE Words
             ([IdWord] INTEGER PRIMARY KEY AUTOINCREMENT, [Content] TEXT UNIQUE, [IdCategory] INTEGER, FOREIGN KEY (IdCategory) REFERENCES "Categories"("IdCategory"))''')
#Inser table
c.execute('''insert into Categories (Name) values ("Muzyka"), ("Zwierzęta"), ("Kraje"), ("Rośliny");''')

c.execute('''insert into Version (Ver, Data) values ("1", datetime());''')
#Inser table
c.execute('''insert into Words (Content, IdCategory) values ("Rock", 1), ("Blues", 1), ("Country", 1), ("Krowa", 2), ("Gepart", 2), ("Aligator", 2), ("Polska", 3), ("Hiszpania", 3), ("Portugalia", 3), ("Róża", 4), ("Tulipan", 4), ("Jabłoń", 4);''')

conn.commit()














